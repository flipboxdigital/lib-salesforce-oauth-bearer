<?php

namespace Flipbox\Guzzle\Plugin\SalesforceBearerAuth\Exception;

use Guzzle\Http\Message\RequestInterface;
use Guzzle\Http\Message\Response;
use Guzzle\Http\Exception\ClientErrorResponseException;

class BearerErrorResponseException extends ClientErrorResponseException
{
    private $bearerReason;
    private $bearerMessage;

    public function getBearerReason()
    {
        return $this->bearerReason;
    }

    public function setBearerReason($bearerReason)
    {
        $this->bearerReason = $bearerReason;
    }

    public function getBearerMessage()
    {
        return $this->bearerMessage;
    }

    public function setBearerMessage($bearerMessage)
    {
        $this->bearerMessage = $bearerMessage;
    }

    public static function factory(RequestInterface $request, Response $response)
    {
        $label = 'Bearer error response';
        $bearerReason = self::bodyToReason($response->json());
        $bearerMessage = self::bodyToMessage($response->json());
        $message = $label . PHP_EOL . implode(PHP_EOL,
            array(
                '[status code] ' . $response->getStatusCode(),
                '[reason phrase] ' . $response->getReasonPhrase(),
                '[bearer reason] ' . $bearerReason,
                '[bearer message] ' . $bearerMessage,
                '[url] ' . $request->getUrl(),
            )
        );

        $e = new static($message);
        $e->setResponse($response);
        $e->setRequest($request);
        $e->setBearerReason($bearerReason);
        $e->setBearerMessage($bearerMessage);

        return $e;
    }

    public static function bodyToReason($body)
    {
        if (null !== $body) {
            foreach ((array)$body as $b) {
                if (isset($b['errorCode'])) {
                    return $b['errorCode'];
                }
            }
        }

        return null;

    }

    public static function bodyToMessage($body)
    {
        if (null !== $body) {
            foreach ((array)$body as $b) {
                if (isset($b['message'])) {
                    return $b['message'];
                }
            }
        }

        return null;

    }

    public static function headerToReason($header)
    {
        if (null !== $header) {
            $p = $header->parseParams();
            foreach ($p as $v) {
                if (isset($v['error'])) {
                    return $v['error'];
                }
            }
        }

        return null;
    }
}
